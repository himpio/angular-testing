import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MiniClockComponent } from './mini-clock.component';

describe('MiniClockComponent', () => {
  let component: MiniClockComponent;
  let fixture: ComponentFixture<MiniClockComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MiniClockComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(MiniClockComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
