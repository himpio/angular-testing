import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-mini-clock',
  templateUrl: './mini-clock.component.html',
  styleUrls: ['./mini-clock.component.css']
})
export class MiniClockComponent implements OnInit {

  public date: Date;

  constructor() {
    this.date = new Date();
  }

  ngOnInit() {
   setInterval(() => {
      this.date = new Date();
    }, 1000);

  }
}


